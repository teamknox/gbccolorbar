# Color bar display for GBC
Color bar is the first program that implemented colorization along with GBC releases. You can learn the basic operation of GBC. 

<table>
<tr>
<td><img src="./pics/colorbar.jpg"></td>
</tr>
</table>


# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 
